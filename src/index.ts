const { program } = require('commander');
const fs = require('fs');
const path = require('path');
const isImage = require('is-image');
const sharp = require('sharp');

import { checkOptions, fileStream, ifFileBeen, ifOptimized } from './helpers';

program
  .version('0.0.1')
  .option('-i, --input <string>', 'input directory')
  .option('-o, --output <string>');

program.parse();

const folder = program.opts().input;
const result = program.opts().output;

const errOptimizeMessage = (item: string) => {
  console.log(`Файл ${item} не удалось преобразовать =(`);
};

interface GetPaths {
  imagePaths: string[];
  filesPaths: string[];
}

const convertToWebp = async (input: string, output: string) => {
  const { imagePaths, filesPaths } = getPaths(input);

  if (imagePaths) {
    const folderPath = path.join(output, 'images');
    await fs.mkdir(folderPath, (err: any) => {
      if (err) throw err;
    });

    imagePaths.forEach(async (item: string) => {
      await optimizeImg(item, folderPath);
    });
  }

  if (filesPaths) {
    const folderPath = path.join(output, 'files');
    await fs.mkdir(folderPath, (err: any) => {
      if (err) throw err;
    });

    filesPaths.forEach((item: string) => {
      const name = path.basename(item);
      if (!ifFileBeen(folderPath, name)) {
        fileStream(item, path.join(folderPath, name));
      }
    });
  }
};

const getPaths = (folder: string): GetPaths => {
  const imagePaths: string[] = [];
  const filesPaths: string[] = [];

  const paths: string[] = [folder];

  while (paths.length > 0) {
    const curPath = paths.shift();
    const curStat = fs.statSync(curPath);

    if (curStat.isDirectory()) {
      const innerPaths = fs.readdirSync(curPath).map((item: string) => {
        return path.join(curPath, item);
      });

      paths.push(...innerPaths);
      continue;
    }

    if (curPath && isImage(curPath)) {
      imagePaths.push(curPath);
      continue;
    } else if (curPath) {
      filesPaths.push(curPath);
    }
  }

  return { imagePaths, filesPaths };
};

const optimizeImg = async (file: string, folderPath: string) => {
  const name = path.basename(file).split('.')[0];
  const convertName = `${file.split('.')[0]}.webp`;
  const newName = path.join(folderPath, `${name}.webp`);
  if (!ifOptimized(folderPath, name)) {
    try {
      await sharp(file).webp({ quality: 70 }).toFile(convertName);
      fileStream(convertName, newName);
    } catch (err) {
      errOptimizeMessage(file);
    }
  }
};

if (checkOptions(folder, result)) {
  convertToWebp(folder, result);
}
