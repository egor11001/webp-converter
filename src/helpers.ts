import { pipeline } from 'node:stream';
const path = require('path');
const fs = require('fs');

const errStreamMessage = (item: string) => {
  console.log(`Файл ${item} не удалось перенести =(`);
};

export const ifOptimized = (folderPath: string, name: string) => {
  return fs.existsSync(path.join(folderPath, `${name}.webp`));
};

export const ifFileBeen = (folderPath: string, name: string) => {
  return fs.existsSync(path.join(folderPath, `${name}`));
};

export const fileStream = (input: string, output: string) => {
  const inputStream = fs.createReadStream(input);
  const outputStream = fs.createWriteStream(output);

  pipeline(inputStream, outputStream, err => {
    err && errStreamMessage(input);
  });
};

export const checkOptions = (folder: string, result: string) => {
  if (!folder && !result) {
    console.log('Write entry and exit points');
  } else if (!folder) {
    console.log('Write entry point');
  } else if (!result) {
    console.log('Write exit point');
  } else if (!fs.existsSync(folder)) {
    console.log('Write correct entry point');
  } else if (!fs.existsSync(result)) {
    console.log('Write correct exit point');
  } else {
    return true;
  }

  return false;
};
